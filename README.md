# mvcbanking-group8 herokuapp

A sample starter app using:

- Node.js platform
- Express web framework
- EJS templating engine
- MVC design pattern
- Mongoose MongoDB object modeling
- Lodash for JavaScript object iteration and manipulation
- jQuery library for DOM manipulation
- BootStrap Material Design framework for responsive design
- nedb In-memory database
- Winston logger

## Links

- Repo <https://mvcbanking-group8.herokuapp.com/>

## Prerequisites

Following must be downloaded, installed, and configured according to the product directions:

1. Node.js and npm
1. Git version control system
1. If Windows, TortoiseGit
1. Heroku


## Code Editor

1. Install Visual Studio Code.

## Get started

Fork this repo into your own cloud account.

Clone your repo down to your local machine.

## Review Code Organization

- app.js - Starting point for the application. Defines the express server, requires routes and models. Loads everything and begins listening for events.
- controllers/ - logic for handling client requests
- data/ - seed data loaded each time the application starts
- models/ - schema descriptions for custom data types
- routes/ - route definitions for the API
- utils/ - utilities for logging and seeding data
- views/ - EJS - embedded JavaScript and HTML used to create dynamic pages

## Install Nodemon Globally

In your new project folder, right-click and "Open PowerShell Here as Administrator". Install nodemon globally to enable live updates.

```PowerShell
> npm install -g nodemon
```

## Install Project Dependencies

Run npm install to install the project dependencies listed in package.json.

```PowerShell
> npm install
```

## Run the App Locally

In your project folder, right-click and "Open PowerShell Here as Administrator". At the prompt, type nodemon app.js to start the server.  (CTRL-C to stop.)

```PowerShell
> nodemon app.js
```

## View Web App

Open browser to the location displayed, e.g. http://localhost:8080/


## Developers Working

1. Vijay Kumar Karanam - User

1. Prajakt UttamRao Khawase - Accounts

1. Sai Kiran - Transactions

1. Harsha Vardhan Reddy Bollam - Categories


