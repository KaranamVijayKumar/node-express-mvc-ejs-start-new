/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrderSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionID: { type: Number, required: true, unique: true, default: 555 },
  transactiondate: { type: Date, required: false },
  accountNumber: {type: Number, required: true},
  transactionamount: { type: Number, required: true, default: 1 }

})

module.exports = mongoose.model('Order', OrderSchema)
