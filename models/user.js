/** 
*  User model
*  Describes the characteristics of each attribute in a user resource.
*
* @author Vijay Kumar Karanam <s534627@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  name: { type: String, required: true },
  accountNumber: {type: Number, required: true, unique: true},
  balance: { type: Number, required: true},
  email: { type: String, required: true, unique: true },
  street1: { type: String, required: true, default: 'Street 1' },
  street2: { type: String, required: false, default: '' },
  city: { type: String, required: true, default: 'Maryville' },
  state: { type: String, required: true, default: 'MO' },
  zip: { type: String, required: true, default: '64468' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('User', UserSchema)
