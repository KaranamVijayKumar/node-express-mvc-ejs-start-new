/** 
*  Category model
*  Describes the characteristics of each attribute in a category - one entry
*
* @author Harshavardhan Reddy Bollam <s534624@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  categoryID: { type: Number, required: true },
  categoryName: { type: String, required: true },
  categoryLimit: { type: Number, required: true },

})

module.exports = mongoose.model('category', categorySchema)
